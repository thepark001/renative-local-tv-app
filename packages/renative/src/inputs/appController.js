import React, { useEffect } from 'react';
import { NavigationContext } from '@react-navigation/native';
// import Api from 'renative/packages/renative/src/Api';
// import { getNavigation } from 'renative/src/navigation';

// function InputsManager() {
const AppController = (props) => {
    console.log('AppController', props);
    const contextType = NavigationContext;
    console.log('InputsManager NavigationActions', contextType);

    useEffect(() => {
        document.addEventListener('keydown', (e) => {
            // Api.navigation.navigate('Program');
            console.log('AppController keyDownHandler props', e);
        });
        // Api.navigation.navigate('Program');
        // console.log('InputsManager keyDownHandler props', this.props);
        // console.log('InputsManager keyDownHandler Api', Api.navigation);
        // console.log('InputsManager keyDownHandler getNavigation', getNavigation());
        // console.log('InputsManager keyDownHandler NavigationContext', NavigationContext);
        //
    }, []);

    return null;
}

export default AppController;
