import React from 'react';
import ReactDOM from 'react-dom';
import App from './src/save/app';

ReactDOM.render(React.createElement(App), document.getElementById('root'));
