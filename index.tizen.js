import React from 'react';
import ReactDOM from 'react-dom';
import App from './src/app';
import 'react-native-gesture-handler';

ReactDOM.render(React.createElement(App), document.getElementById('root'));

(function () {
    var pre = document.createElement('pre');
    pre.setAttribute("id", "log");
    pre.setAttribute("style", "display:block; overflow:auto; padding:5px; margin:0; position:absolute; z-index:10; width:300px; height:auto; min-height:100vh; background-color:rgba(0,0,0,.5); color:#FFFFFF;right:0;");
    var container = document.getElementById('root');
    container.appendChild(pre);
    var old = console.log;
    var logger = document.getElementById('log');
    console.log = function () {
        for (var i = 0; i < arguments.length; i++) {
            if (typeof arguments[i] == 'object') {
                logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
            } else {
                logger.innerHTML += arguments[i] + '<br />';
            }
        }
    }
})();
