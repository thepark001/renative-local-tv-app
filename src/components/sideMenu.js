import React, {useCallback, useEffect, useState, useRef} from 'react';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  Animated,
} from 'react-native';

const window = Dimensions.get('window');
const uri = 'https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png';

const styles = StyleSheet.create({
  menu: {
    position: 'absolute',
    width: 250,
    height: '100%',
    zIndex: 2,
    backgroundColor: 'rgba(0, 0, 0, .9)',
    left: -250,
    padding: 20,
  },
  titleContainer: {
    width: '100%',
    marginBottom: 20,
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 18,
    left: 70,
    top: 20,
    color: 'white',
  },
  item: {
    fontSize: 14,
    fontWeight: '300',
    paddingTop: 5,
    color: 'white',
  },
});

export default function SideMenu() {
  const [pos, setPos] = useState(-250);
  const [state, setState] = useState(false);
  let left = new Animated.Value(-250);

  useEffect(() => {
    state ? opened() : closed();
  }, [closed, opened, state]);

  function opened() {
    left.setValue(-250);
    Animated.timing(left, {
      toValue: 0,
      duration: 500,
    }).start();
  }

  function closed() {
    left.setValue(0);
    Animated.timing(left, {
      toValue: -250,
      duration: 500,
    }).start();
  }

  function keyDownHandler(e) {
    if (e.key === 'o') {
      setState(!state);
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', keyDownHandler);

    return function cleanup() {
      document.removeEventListener('keydown', keyDownHandler);
    };
  });

  return (
    <Animated.ScrollView scrollsToTop={false} style={[styles.menu, {left}]}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Screens Menu</Text>
      </View>

      <Text onPress={() => console.log('About')} style={styles.item}>
        About
      </Text>

      <Text onPress={() => console.log('Contacts')} style={styles.item}>
        Contacts
      </Text>
    </Animated.ScrollView>
  );
}
