import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 10,
    // opacity: 0.2,
    },
    blurState: {
    // opacity: 0.2,
    },
    focusState: {
    // opacity: 1,
    },
    thumb: {
        height: 84,
        minWidth: 150,
    },
});

const ThumbButton = (props) => {
    const [state, setState] = useState({ currentStyle: styles.blurState });
    const { testID, accessible, className, focusable, title } = props;

    // useEffect(() => {
    //    console.log('ThumbButton hasTVPreferredFocus', hasTVPreferredFocus);
    // }, [hasTVPreferredFocus]);

    return (
        <TouchableOpacity
            testID={testID}
            accessible={accessible}
            style={[styles.button, state.currentStyle]}
            onPress={() => {
                props.onPress();
            }}
            onFocus={() => {
                console.log('onFocus');
                setState({ currentStyle: styles.focusState });
                props.onFocus();
            }}
            onBlur={() => {
                setState({ currentStyle: styles.blurState });
            }}
        >
            <Image
                style={styles.thumb}
                source={{ uri: `${props.imageUrl}/scale/width/448` }}
            />
        </TouchableOpacity>
    );
};

export default ThumbButton;

//       hasTVPreferredFocus={hasTVPreferredFocus}
