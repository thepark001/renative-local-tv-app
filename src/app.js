import React, { useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { registerFocusManger, registerServiceWorker } from 'renative';
import Program from './screens/Program';
import Player from './screens/Player';
// import NavigatorService from './services/NavigatorService';

const Drawer = createDrawerNavigator();

// registerFocusManger({ focused: 'opacity: 0.1' });


export default function App() {
    console.log('App init');
    document.addEventListener('keydown', () => {
        console.log('App init document', document);
    });

    // window.addEventListener('keydown', keyDownHandler);



    useEffect(() => {
        console.log('App init');
    }, []);

    return (
        <NavigationContainer>
            <Drawer.Navigator initialRouteName="Program">
                <Drawer.Screen name="Player" component={Player} />
                <Drawer.Screen name="Program" component={Program} />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}

// source NavigationContainer onStateChange
// https://github.com/react-navigation/react-navigation/issues/1439

/*

const getActiveRouteName = (state) => {
    const route = state.routes[state.index];

    if (route.state) {
        // Dive into nested navigators
        return getActiveRouteName(route.state);
    }

    return route.name;
};



*             onStateChange={(state) => {
                console.log('onStateChange state', state);
                const previousRouteName = routeNameRef.current;
                const currentRouteName = getActiveRouteName(state);

                if (previousRouteName !== currentRouteName) {
                    console.log('onStateChange', currentRouteName);
                }
                routeNameRef.current = currentRouteName;
            }
            }
*
* */

