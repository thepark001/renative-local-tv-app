import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textH2: {
        fontFamily: 'TimeBurner',
        fontSize: 20,
        marginHorizontal: 20,
        color: 'purple',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

const ScreenTest = () => {
    console.log('ScreenMyPage');
    document.addEventListener('keydown', (e) => {
        console.log('keydown', e);
    })

    return (
        <View style={styles.container}>
            <Text style={styles.textH2}>
                This is my Page!
            </Text>
        </View>
    );

};

export default ScreenTest;
