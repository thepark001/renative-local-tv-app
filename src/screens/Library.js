import React, { useEffect, useState, Component } from 'react';
import {
    Text,
    Image,
    View,
    Button,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
} from 'react-native';

const styles = StyleSheet.create({
    view: {
        flex: 1,
    },
    title: {
        fontSize: 20,
        marginHorizontal: 20,
        color: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
});

const Library = props => (
    <View style={styles.view}>
        <Text>
            Library Screen hooks
        </Text>
        <Button
            title="Go to Details"
            onPress={() => props.navigation.openDrawer()}
        />
    </View>
);

export default Library;

// onPress={() => props.navigation.openDrawer()}
