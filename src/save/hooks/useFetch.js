import {useEffect, useState} from 'react';

function useFetch(url) {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    async function fetchUrl() {
        const response = await fetch(url);
        const json = await response.json();
        setData(json);
        setLoading(false);
    }
    useEffect(() => {
        fetchUrl();
    }, []);
    return [data, loading];
}

function useFetchArr(arr) {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    async function fetchUrl(url) {
        const response = await fetch(url);
        return await response.json();
    }
    useEffect(() => {
        const getData = async () => {
            return Promise.all(arr.map(url => fetchUrl(url)));
        };
        // eslint-disable-next-line no-shadow
        getData().then((data) => {
            setData(data);
            setLoading(false);
        });
    }, []);
    return [data, loading];
}

export { useFetch, useFetchArr };
