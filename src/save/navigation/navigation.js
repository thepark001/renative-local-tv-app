import React from 'react';
import {
    WEB,
    WEBOS,
    TIZEN,
    getScaledValue,
} from 'renative';

const navStructure = {
    root: {
        menus: {
            drawerMenu: {
                isVisibleIn: [WEB, WEBOS, TIZEN],
                component: 'DrawerMenu',
                options: {
                    drawerBackgroundColor: 'rgba(0,0,0,.9)',
                    drawerColorText: 'white',
                    drawerPosition: 'left',
                    drawerType: 'front',
                    initialRouteName: 'Player',
                    contentOptions: {
                        activeTintColor: 'white',
                        inactiveTintColor: 'gray',
                    },
                },
                navigationOptions: {},
            },
        },
        screens: {
            Player: {
                screen: 'Player',
                navigationOptions: {
                    header: null
                },
                stacks: ['Player'],
            },
            Program: {
                screen: 'Program',
                navigationOptions: {
                    title: 'Program'
                },
                stacks: ['Program'],
            },
        },
    },
    stacks: {
        screens: {
            Player: {
                screen: 'Player',
            },
            Program: {
                screen: 'Program',
            },
        },
        navigationOptions: {
        },
    },
};

export { navStructure };
