import React, { useState, useEffect } from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import Api from 'renative/src/Api';

const hasFocus = Api.formFactor === 'tv' && Api.platform !== 'tvos';
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        border: 'none',
        margin: '10px',
    },
    blurState: {
        opacity: 0.7,
    },
    focusState: {
        opacity: 1,
        border: 'none',
        outline: 'none !important',
    },
    thumb: {
        height: '84px',
        minWidth: '150px',

    }
});

const ThumbButton = (props) => {
    const [state, setState] = useState({ currentStyle: styles.blurState });
    const { testID, accessible, focusable, title } = props;

    useEffect(() => {
        // console.log('ThumbButton focusable', accessible);
    }, [accessible]);

    return (
        <TouchableOpacity
            testID={testID}
            accessible={accessible}
            className="focusable"
            style={[styles.button, state.currentStyle]}
            onPress={() => {
                props.onPress();
            }}
            onFocus={() => {
                if (hasFocus) setState({ currentStyle: styles.focusState });
                props.onFocus();
            }}
            onBlur={() => {
                if (hasFocus) setState({ currentStyle: styles.blurState });
            }}
        >

            <Image
                style={styles.thumb}
                source={`${props.imageUrl}/scale/width/448`}
            />
        </TouchableOpacity>
    );
};

export default ThumbButton;
