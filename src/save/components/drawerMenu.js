import React from 'react';
import { Text, StyleSheet } from 'react-native';
import { Api, registerFocusManger } from 'renative';
import { SafeAreaView } from '@react-navigation/native';
import Button from './button';

const Theme = {
    color1: '#222222',
    color2: '#62DBFB',
    color3: '#FB8D62',
    color4: '#FFFFFF',
    primaryFontFamily: 'TimeBurner'
};

const styles = StyleSheet.create({
    containerVertical: {
        paddingTop: 40,
        paddingLeft: 20,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgb(0,0,0)',
        alignItems: 'center',
        flexDirection: 'column'
    },
    text: {
        color: Theme.color4,
        fontSize: 20,
        marginTop: 10,
        textAlign: 'left',
    },
    button: {
        alignSelf: 'flex-start',
        justifyContent: 'flex-start',
        marginHorizontal: 20,
        maxWidth: 400,
        minWidth: 50,
        borderWidth: 0,
        opacity: 0.5
    },
});

// function keyDownHandler(event) {
//     if (this.props.activeItemKey === 'Player' && event.key === 'ArrowLeft') {
//         try {
//             console.log('keyDownHandler', this.props.navigation.state.isDrawerOpen);
//             this.props.navigation.state.isDrawerOpen
//                 ? this.props.navigation.closeDrawer()
//                 : this.props.navigation.openDrawer();
//         } catch (error) {
//             console.log('openDrawer error', error);
//         }
//     }
// }

class DrawerMenu extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.focus = {};
        // document.addEventListener('keydown', keyDownHandler.bind(this));
    }


    componentDidMount() {
        console.log('drawMenu componentWillUnmount');
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.navigation.state.isDrawerOpen) {
            this.focusManager(true);
        } else {
            this.focusManager(false);
        }
    };

    componentWillUnmount() {
        console.log('drawMenu componentWillUnmount');
    }

    focusManager(isFocus) {
        try {
            this.focus = document.querySelector('.drawerItem0');
            if (this.focus) {
                isFocus ? this.focus.focus() : this.focus.blur();
            }
        } catch (e) {
            console.log('focus ERROR', e);
        }
    }

    render() {
        registerFocusManger({ focused: ' opacity: 1' });
        const {
            navigation,
        } = this.props;

        const { routes } = navigation.state;

        // console.log('drawMenu - isFocused - ', this.props.navigation.isFocused());

        return (
            <SafeAreaView style={[styles.containerVertical, this.props.style]}>
                <Text style={styles.text}>
                    Menu
                </Text>
                {routes.map((route, index) => {
                    const focused = index === navigation.state.index;
                    const id = `drawerItem${index}`;

                    return (
                        <Button
                            key={route.key}
                            id={id}
                            title={route.key}
                            className={['focusable', id]}
                            style={[styles.button, { opacity: focused ? 1 : 0.5 }]}
                            onPress={() => {
                                if (focused) {
                                    Api.navigation.closeDrawer();
                                } else {
                                    Api.navigation.navigate(route.key);
                                }
                            }}
                            onFocus={() => {
                                console.log('onFocus');
                            }}
                            onBlur={() => {
                                console.log('onBlur');
                            }}
                        />
                    );
                })
                }

            </SafeAreaView>
        );
    }
}

export default DrawerMenu;
