import React, { useEffect, useState } from 'react';
import {
    Dimensions,
    StyleSheet,
    View,
    Text,
    Animated,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Button from './button';


const closedValue = -99;
const openedValue = 0;
const window = Dimensions.get('window');
const styles = StyleSheet.create({
    menu: {
        position: 'absolute',
        width: '100vw',
        flex: 1,
        zIndex: 2,
        backgroundColor: 'rgba(0, 0, 0, .9)',
        padding: 10,
    },
    title: {
        fontSize: 18,
        color: 'white',
    },
    button: {
        margin: 10,
        width: '100px',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
});

// let top = -99;//new Animated.Value(closedValue);
// let visibilityValue = 'none';
function TopMenu(props) {
    console.log('TopMenu', props.state);
    const [accessible, setAccessible] = useState(true);
    const [open, setOpen] = useState(false);
    const [visibilityValue, setVisibilityValue] = useState('none');
    // const [top] = useState(new Animated.Value(closedValue));

    // useEffect(() => {
    //     console.log('TopMenu useEffect - top', top);
    //     top = new Animated.Value(closedValue);
    // }, [top]);

    // function opened() {
    //     console.log('TopMenu opened');
    //     top.setValue(-closedValue);
    //     Animated.timing(top, {
    //         toValue: openedValue,
    //         duration: 500,
    //     }).start();
    //     setOpen(true);
    // }
    //
    // function closed() {
    //     console.log('TopMenu closed');
    //     // top.setValue(openedValue);
    //     Animated.timing(top, {
    //         toValue: closedValue,
    //         duration: 500,
    //     }).start();
    //     setOpen(false);
    // }
    //
    // function showSubtitle(item) {
    //     props.textTracks.forEach((textTrack) => {
    //         textTrack.mode = 'hidden';
    //     });
    //     if (item) {
    //         item.mode = 'showing';
    //     }
    // }
    //
    // function keyDownHandler(e) {
    //     console.log('TopMenu - keyDownHandler', e.key);
    //     if (e.key === 'ArrowUp') {
    //         setState(!state);
    //     }
    // }
    //
    useEffect(() => {
        console.log('TopMenu useEffect - init');
        // top = new Animated.Value(closedValue);
        // setOpen(false);
    }, []);

    useEffect(() => {
        console.log('state', props.state);
        setVisibilityValue(props.state ? 'flex' : 'none');
        // eslint-disable-next-line react/destructuring-assignment
        setAccessible(props.state);
        // if (props.state && !open) {
        //     opened();
        // } else if (!props.state && open) {
        //     closed();
        // }
    }, [props.state]);

    // useEffect(() => {
    //     console.log('TopMenu textTracks');
    // }, [props.textTracks]);

    // useEffect(() => {
    //     document.addEventListener('keydown', keyDownHandler);
    //
    //     return function cleanup() {
    //         document.removeEventListener('keydown', keyDownHandler);
    //     };
    // });

    return (

            <View style={[styles.menu, { display: visibilityValue }]}>
                <Text style={styles.title}>Sous titre</Text>
                <View style={styles.buttonContainer}>

                    {props.textTracks.map((texttrack) => {
                        return (
                            <Button
                                onPress={() => {
                                    console.log('onclick', texttrack);
                                    showSubtitle(texttrack);
                                }
                                }
                                accessible={accessible}
                                style={styles.button}
                                key={texttrack.language}
                                title={texttrack.language}
                            />
                        );
                    })}
                </View>
            </View>
        // </Animated.View>
    );
}

export default withNavigation(TopMenu);
//<Animated.View style={[styles.menu, { top }]}> </Animated.View>
