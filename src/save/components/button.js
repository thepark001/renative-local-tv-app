import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import Api from 'renative/src/Api';

const hasFocus = Api.formFactor === 'tv' && Api.platform !== 'tvos';
const styles = StyleSheet.create({
    button: {
        marginTop: 30,
        marginHorizontal: 20,
        height: 20,
        minWidth: 150,
        maxWidth: 200,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    buttonText: {
        fontFamily: 'TimeBurner',
        color: '#FFFFFF',
        fontSize: 20,
    },
    blurState: {
        opacity: 0.7,
    },
    focusState: {
        opacity: 1,
        border: 'none',
        outline: 'none !important',
    },
});

const Button = (props) => {
    const [state, setState] = useState({ currentStyle: styles.blurState });
    const { testID, accessible, title } = props;

    useEffect(() => {
        console.log('ThumbButton focusable', accessible);
    }, [accessible]);

    return (
        <TouchableOpacity
            testID={testID}
            accessible={accessible}
            className="focusable"
            style={[styles.button, state.currentStyle]}
            onPress={() => {
                props.onPress();
            }}
            onFocus={() => {
                if (hasFocus) setState({ currentStyle: styles.focusState });
            }}
            onBlur={() => {
                if (hasFocus) setState({ currentStyle: styles.blurState });
            }}
        >
            <Text style={styles.buttonText}>
                {title}
            </Text>
        </TouchableOpacity>
    );
}

export default Button;
