import React, { useEffect, useState } from 'react';
import { registerFocusManger, registerServiceWorker } from 'renative';
import TopMenu from '../components/topMenu';

const styles = {
    videoPlayer: {
        position: 'fixed',
        alignSelf: 'center',
        width: '100vw',
        height: '100vh',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        border: 'none',
        margin: '10px',
    }
};

let SRGLetterbox = null;
let srgLetterboxPlayer = null;
let isLocal = true;

function improveLayoutIfNeeded() {
    if (srgLetterboxPlayer) {
        srgLetterboxPlayer.player.ready(() => {
            const videoEl = document.querySelector('.vjs-srgssr-skin');
            if (videoEl && videoEl.classList.contains('vjs-fill')) {
                videoEl.classList.remove('vjs-fill');
                videoEl.classList.add('vjs-fluid');
            }
        })
    }
}

function playURN(urn) {
    console.log('playURN');
    srgLetterboxPlayer.setPlayerFocus(false);
    if (srgLetterboxPlayer.getUrn() === urn) {
        srgLetterboxPlayer.paused() ? srgLetterboxPlayer.play() : srgLetterboxPlayer.pause();
    }else {
        srgLetterboxPlayer.prepareToPlayURN(urn, undefined, undefined, 'any');
        // srgLetterboxPlayer.play();
    }
}

/*
* Work around to remove renative script for focused element
**/
function removeFocusedScript() {
    const focusedStyle = document.createElement('style');
    focusedStyle.rel = 'stylesheet';
    focusedStyle.type = 'text/css';
    focusedStyle.appendChild(
        document.createTextNode(`:focus {
          border: none !important;
}
`),
    );
    document.head.appendChild(focusedStyle);
}

const Player = (props) => {
    const baseUrl = '//letterbox-web-2.s3.amazonaws.com/stage'; // develop
    // const baseUrl = '//letterbox-web-2.s3.amazonaws.com/test_rinaldo';
    const script = document.createElement('script');
    const [textTracks, setTextTracks] = useState([]);
    const topMenuRef = React.createRef();

    const [state, setState] = useState(false);
    const [topMenu, setTopMenu] = useState(false);
    const [drawer, setDrawer] = useState(false);


    function keyDownHandler(e) {
        console.log('Player - keyDownHandler', drawer);
        if (e.key === 'ArrowUp' && drawer === false) {
            setTopMenu(!topMenu);
        }
    }

    function initializePlayer() {
        console.log('initializePlayer');
        const optionsParam = {
            debug: false,
            ilHost: 'il.srgssr.ch',
            // ilHost: 'play-mmf.herokuapp.com',
            fillMode: true,
            playerFocus: false,
            hdMode: undefined,
            language: 'en',
            controls: false,
            controlBar: false,
            headerComponent: false,
            muted: false,
            subdivisionsContainer: false,
        };

        srgLetterboxPlayer = new window.SRGLetterbox(optionsParam);

        srgLetterboxPlayer.playerOptions.controls = false;
        srgLetterboxPlayer.playerOptions.muted = true;
        srgLetterboxPlayer.playerOptions.debug = false;
        srgLetterboxPlayer.playerOptions.playerFocus = false;
        srgLetterboxPlayer.playerOptions.userActions = {};
        srgLetterboxPlayer.playerOptions.headerComponent = false;

        srgLetterboxPlayer.initializeOnElement('letterboxWebPlayer');

        srgLetterboxPlayer.player.on('canplay', () => {
            setTextTracks(window.videojs.getPlayers().vjs_video_3.textTracks().tracks_);
            try {
                const event = new CustomEvent('APP_READY', {});
                document.dispatchEvent(event);
            }catch (e) {
                console.log('APP_READY ERROR', e);
            }
        });
        // playURN('urn:swi:video:44927366'); // rats
        // playURN('urn:rts:video:10719425') // subtitle
        // playURN('urn:rts:video:3608506'); // rts live
        // playURN('urn:srf:video:c4927fcf-e1a0-0001-7edd-1ef01d441651'); // srf live
        // playURN('urn:rts:video:9314220');
        playURN('urn:swi:video:44927366'); // swi mp4
        // srgLetterboxPlayer.prepareToPlayURN('urn:swi:video:44927366'); // swi mp4
        // srgLetterboxPlayer.prepareToPlayURN('urn:rts:video:_andreteststream1'); // hls
        // srgLetterboxPlayer.prepareToPlayURN('urn:rts:video:3608506'); // rts live

        // improveLayoutIfNeeded();
    }

    useEffect(() => {
        console.log('useEffect textTracks', textTracks);
        if (textTracks.length > 0) {
            console.log('textTracks', textTracks);
        }
    }, [textTracks]);

    useEffect(() => {
        console.log('PLAYER - useEffect init');
        removeFocusedScript();

        // script.async = true;
        // script.src = `${baseUrl}/letterbox.js`;
        // script.type = 'text/javascript';
        //
        // script.onload = initializePlayer;
        //
        // document.body.appendChild(script);

        let link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = `${baseUrl}/letterbox.css`;
        link.media = 'all';

        document.head.appendChild(link);

        initializePlayer();

        // TODO fix topMen disabled when drawer is open
        try {
            props.navigation.addListener('action', (e) => {
                console.log('Player - addListener action', e.action.type);
                switch (e.action.type) {
                    case 'Navigation/DRAWER_CLOSED':
                        setDrawer(false);
                        break;
                    case 'Navigation/OPEN_DRAWER':
                        setDrawer(true);
                        if (state) {
                            setState(false);
                        }
                        break;

                }
            });
        } catch (e) {
            console.log('Player - DRAWER_CLOSED ERROR', e);
        }

        try {
            props.navigation.addListener('didFocus', (e) => {
                if (e.action.params !== undefined && e.action.params.urn !== undefined && props.navigation.isFocused()){
                    playURN(e.action.params.urn);
                }
            });

            props.navigation.addListener('willBlur', (e) => {
                console.log('Player willBlur - ', props.navigation.isFocused());
            });
        } catch (e) {
            console.log('Player - didFocus willBlur ERROR', e);
        }

        registerFocusManger({ focused: 'opacity: 1' });
        registerServiceWorker();

    }, []);

    useEffect(() => {
        document.addEventListener('keydown', keyDownHandler);

        return function cleanup() {
            document.removeEventListener('keydown', keyDownHandler);
        };
    });

    useEffect(() => {
        // console.log('Player useEffect  navigation ', props.navigation);
    }, [props.navigation]);

    return (

        <React.Fragment>
            <TopMenu ref={topMenuRef} playerProps={props} textTracks={textTracks} state={topMenu}/>
            <div accessible="false" id="letterboxWebPlayer" style={styles.videoPlayer} tabIndex={-1} />
        </React.Fragment>
    );
};

export default Player;
