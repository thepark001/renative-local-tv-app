import React, { useEffect, useState, useRef, createRef } from 'react';
import { Api } from 'renative';
import { Text, View, ScrollView } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import ThumbButton from '../components/thumbButton';
import { useFetchArr } from '../hooks/useFetch';


console.log('Program page --- INIT');
const styles = {
    title: {
        fontSize: '20px',
        margin: '20px',
    },
    button: {
        marginTop: 10,
        minWidth: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    buttonContainer: {
        width: '100vw',
        // display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
        // justifyContent: 'left',
        // flexWrap: 'wrap',
        margin: '20px',

    },
};
const renderElement = <div><Text style={styles.title}> Loading ! </Text></div>;
const mediaListSWIUrl = 'http://il.srgssr.ch/integrationlayer/2.0/swi/mediaList/video/mostClicked';
const mediaListRTSUrl = 'http://il.srgssr.ch/integrationlayer/2.0/rts/mediaList/video/mostClicked';
const mediaListRTSLIVEUrl = 'http://il.srgssr.ch/integrationlayer/2.0/rts/mediaList/video/livestreams';
const mediaListSRFUrl = 'http://il.srgssr.ch/integrationlayer/2.0/srf/mediaList/video/mostClicked';
const mediaListAudioLiveRTSUrl = 'https://il.srgssr.ch/integrationlayer/2.0/rts/mediaList/audio/livestreams';
const dataLoading = undefined;
const data = undefined;
let isDrawerOpen = false;

const Program = (props) => {
    console.log('Program INIT');
    const indexRef = useRef(0);
    const focusable = useRef(true);
    const [accessible, setAccessible] = useState(true);
    // const isDrawerOpen = useIsDrawerOpen();
    const scrollViewRef = createRef();
    let swimLaneRef0 = createRef();
    let swimLaneRef1 = createRef();
    let swimLaneRef2 = createRef();


    // TODO : test focus on focus  listener (useFocusEffect)
    // useFocusEffect(
    //     React.useCallback(() => {
    //         console.log('useCallback');
    //         document.addEventListener('keydown', function(){
    //             console.log('keydown');
    //         });
    //
    //         return () => {
    //             // Do something when the screen is unfocused
    //             // Useful for cleanup functions
    //         };
    //     }, [])
    // );



    function createThumbButtons(arr) {
        return arr.map((data, index) => (
            <ThumbButton
                key={data.title}
                accessible={accessible}
                style={styles.button}
                title={data.title}
                className="focusable"
                imageUrl={data.imageUrl}
                onPress={() => {
                    Api.navigation.navigate('Player', { urn: data.urn });
                }}
                onFocus={() => {
                    indexRef.current = index;
                    // try {
                    //     // scrollViewRef
                    //     const swimLaneTargeted = findNodeHandle(swimLaneRef0);
                    //     console.log('test scrolll swimLaneTargeted', swimLaneTargeted);
                    //     this.scrollViewRef.measureLayout(swimLaneTargeted, (x, y, width, height) => {
                    //         console.log('got measurement', x, y, width, height);
                    //     });
                    // } catch (e) {
                    //     console.log('test scrolll error', e);
                    // }
                }}
            />
        ));
    }

    function closeDrawer() {
        props.navigation.closeDrawer();
        focusable.current = true;
        setAccessible(true);
    }

    function openDrawer() {
        props.navigation.openDrawer();
        focusable.current = false;
        setAccessible(false);
    }

    function keyDownHandler(event) {
        console.log('Program - keyDownHandler isDrawerOpen', isDrawerOpen);
        if (event.key === 'ArrowLeft' && indexRef.current === 0) {
            try {
                // eslint-disable-next-line no-unused-expressions
                isDrawerOpen === true
                    ? closeDrawer()
                    : openDrawer();
            } catch (error) {
                console.log('openDrawer error', error);
            }
        }
    }

    useEffect(() => {
        console.log('Program useEffect for init');

        // eslint-disable-next-line no-const-assign,max-len
        [data, dataLoading] = useFetchArr([mediaListSWIUrl, mediaListRTSUrl, mediaListRTSLIVEUrl, mediaListSRFUrl, mediaListAudioLiveRTSUrl]);
        window.addEventListener('keydown', keyDownHandler);

        props.navigation.addListener('action', (e) => {
            console.log('Action type', e.action.type);
            switch (e.action.type) {
                case 'Navigation/DRAWER_CLOSED':
                    setAccessible(true);
                    isDrawerOpen = false;
                    break;
                case 'Navigation/DRAWER_OPENED':
                    isDrawerOpen = true;
                    break;
                default:
                    break;
            }
            // if (e.action.type === 'Navigation/DRAWER_CLOSED') {
            //     setAccessible(true);
            // }
        });
    }, []);

    useEffect(() => {
        console.log('Program useEffect for init');
        // setAccessible(true);
    });

    return (
        <ScrollView ref={scrollViewRef}>
            {dataLoading ? renderElement : (
                <View>
                    <Text style={styles.title}>SWI most clicked</Text>
                    <View style={styles.buttonContainer} ref={ref => (swimLaneRef0 = ref)}>
                        {createThumbButtons(data[0].mediaList)}
                    </View>

                    <Text style={styles.title}>RTS most clicked</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[1].mediaList)}
                    </View>

                    <Text style={styles.title}>RTS Live</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[2].mediaList)}
                    </View>

                    <Text style={styles.title}>SRF most clicked</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[3].mediaList)}
                    </View>

                    <Text style={styles.title}>RTS audio live</Text>
                    <View style={styles.buttonContainer}>
                        {createThumbButtons(data[4].mediaList)}
                    </View>
                </View>
            )}
        </ScrollView>
    );
};

export default Program;
