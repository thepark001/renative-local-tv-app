import React from 'react';
import { createApp, registerServiceWorker, Api } from 'renative';
import { navStructure } from './navigation/navigation';
import Player from './screens/Player';
import Program from './screens/Program';
import DrawerMenu from './components/drawerMenu';
import SplashScreen from './screens/splashScreen';
import KeyEvent from 'react-native-keyevent';
import '../../letterBoxWeb/letterbox';


import '../../platformAssets/runtime/fontManager';

registerServiceWorker();

let AppContainer;

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loading: true };
        console.log('App', Api.platform);
        AppContainer = createApp(navStructure, { Player, Program, DrawerMenu });

        AppContainer.onNavigationStateChange = (e) => {
            console.log('AppContainer onNavigationStateChange', e);
        };

        console.log('AppContainer', AppContainer);

        try {
            document.addEventListener('APP_READY', () => {
                console.log('APP_READY');
                this.setState({ loading: false });
            });
        } catch (e) {
            console.log('LISTENER APP_READY ERROR', e);
        }
    }

    render() {
        return AppContainer;
    }
}

export default App;
