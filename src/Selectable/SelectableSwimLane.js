/* @flow */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import keyListener from './keyListener';
import keyCodes from './keyCodes';

type TPosition = {
  x: number,
  y: number,
};

type TSelectable = {
  x: number,
  y: number,
  width: number,
  height: number,
  onFocus: Function,
  onPress: Function,
  onBlur: Function,
};

type TState = {
  activeSelectable: ?TSelectable,
  selectables: Array<TSelectable>,
  isSelectable: boolean,
};

type TProps = {
  children?: any,
};

export default class SelectableSwimLane extends Component {
  static childContextTypes = {
    registerSelectable: PropTypes.func,
  };

  state: TState = {
    activeSelectable: null,
    selectables: [],
    isSelectable: true,
  };

  props: TProps;

  _listenerKeyDown: ?Function;

  selectedIndex = 0;
  sortedSelectables = [];
  isFocused = false;

  componentDidMount() {
    // this._listenerKeyDown = keyListener.set(this._handleKeyDown);
  }

  componentWillUnmount() {
    // keyListener.remove(this._listenerKeyDown);
  }

  componentWillReceiveProps() {
    // console.log('SelectableSwimLane - componentWillReceiveProps', this.props.isFocused);
    this.isFocused = this.props.isFocused;
    if (
      this.isFocused &&
      this.sortedSelectables.length === 0 &&
      this.state.selectables.length > 0
    ) {

      this.sortedSelectables = this.state.selectables;
      this.sortedSelectables[this.selectedIndex].onFocus();
      this.setState({
        activeSelectable: this.sortedSelectables[this.selectedIndex],
      });
    }

    // this._listenerKeyDown = keyListener.set(
    //   this._handleKeyDown,
    //   this._listenerKeyDown,
    // );
  }

  getChildContext() {
    return {
      registerSelectable: this._registerSelectable,
    };
  }

  selectNewActive(direction) {
    if (this.state.activeSelectable) {
      this.state.activeSelectable.onBlur();
    }
    if (direction === keyCodes.right) {
      this.selectedIndex =
        this.selectedIndex === this.sortedSelectables.length - 1
          ? this.sortedSelectables.length - 1
          : this.selectedIndex + 1;
    } else if (direction === keyCodes.left) {
      this.selectedIndex =
        this.selectedIndex === 0 ? 0 : this.selectedIndex - 1;
    }
    this.sortedSelectables[this.selectedIndex].onFocus();
    this.setState({
      activeSelectable: this.sortedSelectables[this.selectedIndex],
    });
  }

  newActiveOnPress() {
    this.state.activeSelectable.onPress();
  }

  // _handleKeyDown = (key: number) => {
  //   switch (key.keyCode) {
  //     case keyCodes.right:
  //     case keyCodes.left:
  //       this.selectNewActive(key.keyCode);
  //       break;
  //     case keyCodes.center:
  //       if (this.state.activeSelectable) {
  //         this.state.activeSelectable.onPress();
  //       }
  //       break;
  //   }
  //
  //   return true;
  // };

  _registerSelectable = (
    position: TPosition,
    onFocus: Function,
    onBlur: Function,
    onPress: Function,
  ) => {
    this.state.selectables.push({
      x: position.x,
      y: position.y,
      width: position.width,
      height: position.height,
      onFocus,
      onBlur,
      onPress,
    });
    this.setState({selectables: this.state.selectables});
  };

  render() {
    const {children} = this.props;
    return <View>{children}</View>;
  }
}
