/* @flow */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import keyListener from './keyListener';
import keyCodes from './keyCodes';

type TProps = {
  onFocus?: Function,
  onBlur?: Function,
  onNavigate?: Function,
  onPress?: Function,
};

type TState = {
  isFocused: boolean,
  registered: boolean,
};

type TContext = {
  registerSelectable: Function,
};

export default function selectableContainer(WrappedComponent: any) {
    return class SelectableContainer extends Component {
    static contextTypes = {
        registerSelectable: PropTypes.func,
    };

    state: TState = {
        isFocused: false,
        registered: false,
    };

    props: TProps;

    context: TContext;

    _wrappedComponent: any;

    _handleFocus = () => {
        const { onFocus } = this.props;

        this.setState({ isFocused: true });

        if (onFocus) {
            onFocus();
        }
    };

    _handleBlur = () => {
        const { onBlur } = this.props;

        this.setState({ isFocused: false });

        if (onBlur) {
            onBlur();
        }
    };

    // TODO : set state to avoid child function call
    _handleNavigation = (direction) => {
        this._wrappedComponent.selectNewActive(direction);
    };

    // TODO : set state to avoid child function call
    _handlePress = () => {
        this._wrappedComponent.newActiveOnPress();
    };

    _handleLayout = (e: Object) => {
        if (this.state.registered) {
            return;
        }

        this.context.registerSelectable(this._handleFocus, this._handleBlur, this._handleNavigation, this._handlePress);
        this.setState({ registered: true });
    };

    render() {
        return (
            <View onLayout={this._handleLayout}>
                <WrappedComponent
                    isFocused={this.state.isFocused}
                    ref={x => (this._wrappedComponent = x)}
                    {...this.props}
                />
            </View>
        );
    }
    };
}
