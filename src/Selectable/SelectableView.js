import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, findNodeHandle, ScrollView } from 'react-native';
import keyListener from './keyListener';
import keyCodes from './keyCodes';

type TSelectable = {
  onFocus: Function,
  onBlur: Function,
  onNavigate: Function,
  onPress: Function,
};

type TState = {
  activeSelectable: ?TSelectable,
  selectables: Array<TSelectable>,
  isSelectable: boolean,
};

type TProps = {
  children?: any,
};

export default class SelectableView extends Component {
  static childContextTypes = {
      registerSelectable: PropTypes.func,
  };

  state: TState = {
      activeSelectable: null,
      selectables: [],
      isSelectable: true,
  };

  props: TProps;

  _listenerKey: ?Function;

  selectedIndex = 0;

  sortedSelectables = [];

  componentDidMount() {
      console.log('SelectableView componentDidMount', this.props.isFocused);
      this.isFocused = this.props.isFocused;
      this._listenerKey = keyListener.set(this._handleKeyDown);
  }

  componentWillUnmount() {
      keyListener.remove(this._listenerKey);
  }

  componentWillReceiveProps() {
      if (
          this.sortedSelectables.length === 0
      && this.state.selectables.length > 0
      ) {
          this.sortedSelectables = this.state.selectables;
          try {
              this.sortedSelectables[this.selectedIndex].onFocus();
              this.setState({
                  activeSelectable: this.sortedSelectables[this.selectedIndex],
              });
          } catch (e) {
              console.log('SelectableView componentWillReceiveProps - ERROR', e);
          }
      }

      if (this._listenerKey === null){
          this._listenerKey = keyListener.set(this._handleKeyDown, this._listenerKey);
      }
          console.log('SelectableView componentWillReceiveProps - this._listenerKey ', this._listenerKey );





  }

  getChildContext() {
      return {
          registerSelectable: this._registerSelectable,
      };
  }

  _selectSwimLaneNavigation(direction) {
      this.sortedSelectables[this.selectedIndex].onNavigate(direction);
  }

  _changeSwimLane(direction) {
      this.sortedSelectables[this.selectedIndex].onBlur();
      if (direction === keyCodes.down) {
          this.selectedIndex = this.selectedIndex === this.sortedSelectables.length - 1
              ? this.sortedSelectables.length - 1
              : this.selectedIndex + 1;
      } else if (direction === keyCodes.up) {
          this.selectedIndex = this.selectedIndex === 0 ? 0 : this.selectedIndex - 1;
      }
      this.sortedSelectables[this.selectedIndex].onFocus();
      console.log(
          'check item',
          findNodeHandle(this.sortedSelectables[this.selectedIndex]),
      );
      this.setState({
          activeSelectable: this.sortedSelectables[this.selectedIndex],
      });
  }

  _handleKeyDown = (key: number) => {
      console.log('SelectableView _handleKeyDown', key);
      switch (key.keyCode) {
          case keyCodes.right:
          case keyCodes.left:
              this._selectSwimLaneNavigation(key.keyCode);
              break;
          case keyCodes.up:
          case keyCodes.down:
              this._changeSwimLane(key.keyCode);
              break;
          case keyCodes.center:
              if (this.state.activeSelectable) {
                  this.state.activeSelectable.onPress();
              }
              break;
      }

      return true;
  };

  _registerSelectable = (
      onFocus: Function,
      onBlur: Function,
      onNavigate: Function,
      onPress: Function,
  ) => {
      this.state.selectables.push({
          onFocus,
          onBlur,
          onNavigate,
          onPress,
      });
      this.setState({ selectables: this.state.selectables });
  };

  render() {
      const { children } = this.props;
      return (
          <ScrollView>
              {children}
          </ScrollView>
      );
  }
}
